class ENOKERNEL(Exception):

    pass

class ENOUSER(Exception):

    pass

class NOTIMPLEMENTED(Exception):

    pass

class ETYPE(Exception):

    pass

class ENOCLASS(Exception):

    pass

class ENOFILENAME(Exception):

    pass
